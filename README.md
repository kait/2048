# 2048小程序

在别人的基础上改进的2048小程序，打算作为商用，但是2048小程序属于小游戏类别，微信小游戏对个人开发者不太友好，遂把代码公布在网上，供别人学习参考！

请尊重别人的劳动成功，在使用前请先 Fork 或 Star


## 截图

![输入图片说明](https://gitee.com/uploads/images/2018/0423/123841_c48d5d3a_1767084.png "1.PNG")
![输入图片说明](https://gitee.com/uploads/images/2018/0423/123912_a557d879_1767084.png "2.PNG")
![输入图片说明](https://gitee.com/uploads/images/2018/0423/123919_63d59131_1767084.png "3.PNG")
![输入图片说明](https://gitee.com/uploads/images/2018/0423/123928_c3f5f1e4_1767084.png "4.PNG")
![输入图片说明](https://gitee.com/uploads/images/2018/0423/123937_d7d3a84f_1767084.png "5.PNG")
![输入图片说明](https://gitee.com/uploads/images/2018/0423/123944_d23cc666_1767084.png "6.PNG")
